<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - Free Bare
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
//devavrata theme settings
require('devavrata_core/devavrata_free_bare_settings.php');
//devavrata engines
require('devavrata_core/devavrata_free_bare_region_engine.php');
//devavrata column engines
require('devavrata_core/devavrata_free_bare_4column_engine.php');
//devavrata content engines
require('devavrata_core/devavrata_free_bare_content_engine.php');

