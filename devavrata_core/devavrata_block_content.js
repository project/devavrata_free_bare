/***********************************************************
 * Devavrata Core Theme for Drupal
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
//block content show
$(document).ready(function() {
	//$('.block-panel').corner('5px');
	//$('.block-title').corner('5px');
	$('.block').bind('dblclick', function() {
		$('.sidebar .block .content').slideUp('normal').removeClass('block-show');
		$('.sidebar .block').removeClass('block-unselected').removeClass('block-selected').removeClass('selected');
	});
	$('.block').bind('click', function() {
		$('.sidebar .block-unselected .content').slideUp('normal').removeClass('block-show');
		$(this).removeClass('block-unselected').addClass('block-selected').addClass('selected');
		$('.block-selected .content').slideDown('normal').toggleClass('block-show');
		$('.block').addClass('block-unselected').removeClass('block-selected').removeClass('selected');
		$(this).toggleClass('selected').removeClass('block-unselected');
		$('.sidebar .block-unselected .content').slideUp('normal').removeClass('block-show');
	});
});
