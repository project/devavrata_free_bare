<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - Advanced
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
/* Apply page size
 */
function devavrata_free_bare_wrapper_size($sidebar_check, $wrapper_id, $wrapper_class, $page_size) {
	return '<div id="' . $wrapper_id . $sidebar_check . $page_size . '" class="' . $wrapper_class . '">'; 
}
/* Check for region existence and returs 3 settings
 * -twobars => with left & right sidebars
 * -full    => without left & right sidebars
 * -onebar  => with left or right sidebar
 */
function devavrata_free_bare_region_sidebar_check($sidebar_left = 0, $sidebar_right = 0) {
	$sidebar_left = implode('', $sidebar_left);
	$sidebar_right = implode('', $sidebar_right);
	if ($sidebar_left && $sidebar_right) { return '-twobars'; } 
	elseif(!$sidebar_left && !$sidebar_right) { return '-full'; } 
	elseif($sidebar_left && !$sidebar_right) { return '-onebar-left'; } 
	elseif(!$sidebar_left && $sidebar_right) { return '-onebar-right'; } 
	else {  return '-onebar'; }
}

/* Process regions
 */
function devavrata_free_bare_region($region = '', $region_id = '', $region_class = '') {
	if ($region) { return '<div id="' . $region_id . '" class="' . $region_class . '">' . $region . '</div>'; }
}
/* Process collection of regions
 */
function devavrata_free_bare_wrapper_region($sidebar_content = '', $wrapper_region_id = '',  $wrapper_region_class = '') {
	$show_content = implode('', $sidebar_content);
	if($show_content) {
		$counter = 1;	$wrapper_region  = '';
		foreach($sidebar_content as $sidebar_key => $sidebar_content_show) {
			$wrapper_region .=  devavrata_free_bare_region($sidebar_content_show, $region_id = $wrapper_region_id . $counter++, $region_class = $wrapper_region_class);
		}
		return $wrapper_region;
	}
}

