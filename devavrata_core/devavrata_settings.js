/***********************************************************
 * Devavrata Core Theme for Drupal
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/

$(document).ready( function() {
  // Hide settings if unchecked
  $('#edit-devavrata-settings').change(
    function() {
      div = $('#devavrata-settings-collapse');
      if ($('#edit-devavrata-settings').attr('checked') == '') {
        div.slideUp('slow');
      } else  {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-devavrata-settings').attr('checked') == '') {
    $('#devavrata-settings-collapse').css('display', 'none');
  } else {
	  $('#devavrata-settings-collapse').css('display', 'block');
  }
});
