<?php 
/***********************************************************
 * Devavrata Core Theme for Drupal - Free Bare Template
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
	<?php print $head ?>
	<title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if IE 6]><?php print devavrata_free_bare_get_ie6_styles(); ?><![endif]-->
    <!--[if IE 7]><?php print devavrata_free_bare_get_ie7_styles(); ?><![endif]-->
<?php 	//get saved/default settings
// DEVAVRATA LAYOUT SETTINGS
		$devavrata_settings = theme_get_setting('devavrata_settings'); 
		if($devavrata_settings == 0) { devavrata_initialize(); };
		$devavrata_path = drupal_get_path('theme','devavrata_free_bare'); 
		
		$body_image = theme_get_setting('body_image'); 
		$body_image_position = theme_get_setting('body_image_position'); 
		$page_size = theme_get_setting('page_size'); 
// DEVAVRATA CONTENT FORMAT SETTINGS
		$content_title_size = theme_get_setting('content_title_size');
		$content_title_align = theme_get_setting('content_title_align');
		$content_body_size = theme_get_setting('content_body_size');
		$content_body_margin = theme_get_setting('content_body_margin');
// DEVAVRATA UTILITY SETTINGS
		$sitename_shadow = theme_get_setting('sitename_shadow'); 
		$block_content = theme_get_setting('block_content');

		//sidebars
		$sidebar_left_inside_content = array($left);
		$sidebar_right_inside_content = array($right);
		$sidebar_left_outside_content = array($left_outside); 
		$sidebar_right_outside_content = array($right_outside); 
		//check regions existence for pages sizes
		$sidebar_inside_check = devavrata_free_bare_region_sidebar_check($sidebar_left_inside_content, $sidebar_right_inside_content);
		$sidebar_outside_check = devavrata_free_bare_region_sidebar_check($sidebar_left_outside_content, $sidebar_right_outside_content); 
		//fuse all blocks inside the sidebars
		$left_outside_wrapper = devavrata_free_bare_wrapper_region($sidebar_left_outside_content, $wrapper_region_id = 'left-outside', $wrapper_region_class = 'left-sidebar-outside sidebar-outside sidebar sub-section');
   	$right_outside_wrapper = devavrata_free_bare_wrapper_region($sidebar_right_outside_content, $wrapper_region_id = 'right-outside', $wrapper_region_class = 'right-sidebar-outside sidebar-outside sidebar sub-section');
		$left_inside_wrapper = devavrata_free_bare_wrapper_region($sidebar_left_inside_content, $wrapper_region_id = 'left', $wrapper_region_class = 'left-sidebar sidebar sub-section');
      $right_inside_wrapper = devavrata_free_bare_wrapper_region($sidebar_right_inside_content, $wrapper_region_id = 'right', $wrapper_region_class = 'right-sidebar sidebar sub-section');
		//fuse all blocks inside the sidebars for column layout
		$left_inside_wrapper_column = devavrata_free_bare_wrapper_region($sidebar_left_inside_content, $wrapper_region_id = 'left', $wrapper_region_class = ' sub-section');
      $right_inside_wrapper_column = devavrata_free_bare_wrapper_region($sidebar_right_inside_content, $wrapper_region_id = 'right', $wrapper_region_class =  'sub-section');
		//header contents
		$header_region = devavrata_free_bare_header_region($logo, $site_name, $site_slogan, $search_box, $header, $front_page, $sitename_shadow);
		//footer contents
		$footer_region = devavrata_free_bare_footer($footer_message, $footer); //$feed_icons
		//page width settings
		$page_width_setting = devavrata_free_bare_width_settings($page_size);
		//print styles here
	  	print devavrata_free_bare_layout_feature($body_image, $body_image_position);
	  	print devavrata_free_bare_text_feature($content_title_size, $content_title_align, $content_body_size, $content_body_margin);
	  	print devavrata_free_bare_utility_feature($block_content);
?>
</head>
<body class="<?php print $body_classes; ?>">
<?php print devavrata_free_bare_wrapper_size($sidebar_outside_check, $wrapper_id = 'page-wrapper', $wrapper_class = 'page-wrapper', $page_width_setting); ?>
    	<?php print devavrata_free_bare_region($left_outside_wrapper, $region_id = 'left-outside-wrapper', $region_class = 'sidebar-outside-wrapper wrapper main-section'); ?>
    	<?php print '<div id="main-wrapper' . devavrata_free_bare_width_settings($page_size) . '" class="main-wrapper wrapper main-section">'; ?>
        	<?php devavrata_free_bare_logged_in_var($logged_in); devavrata_free_bare_is_admin_var($is_admin); ?>
         <?php print devavrata_free_bare_region($header_region, $region_id = 'header-region-normal', $region_class = 'header-region-main header-region block-outline block-panel sub-section'); ?>
         <?php print devavrata_free_bare_region($header_bottom, $region_id = 'header-region-bottom', $region_class = 'header-region block-outline block-panel sub-section'); ?>
       	<?php print devavrata_free_bare_menubar_links($primary_links_tree, $primary_links, $primary_links_id = 'primary-links', $primary_links_class = 'sub-section'); ?>
        	<?php print devavrata_free_bare_menubar_links($secondary_links_tree, $secondary_links, $secondary_links_id = 'secondary-links', $secondary_links_class = 'sub-section'); ?>
        	<?php print devavrata_free_bare_column_region($preface1, $preface2, $preface3, $preface4, $column_region = 'Preface', $is_admin, $_GET['column-state'], $page_width_setting); ?>
        	<div id="content-wrapper" class="wrapper">
				<?php print devavrata_free_bare_region($left_inside_wrapper, $region_id = 'left-wrapper', $region_class = 'sidebar-wrapper wrapper'); ?>
            <?php print devavrata_free_bare_wrapper_size($sidebar_inside_check, $wrapper_id = 'content-main', $wrapper_class = 'content-main sub-section', $page_width_setting); ?>
                  <div id="content-inner-wrapper" class=" block-outline block-panel wrapper">
                  <?php print devavrata_free_bare_region($content_top, $region_id = 'content-region-top', $region_class = 'content-region sub-section'); ?>
                  <?php print devavrata_free_bare_content_elements($breadcrumb,$mission,$title,$tabs,$tabs2,$messages,$help); ?>
                  <?php print devavrata_free_bare_region($content, $region_id = 'content-region', $region_class = 'content-region sub-section'); ?>
               <div class="block-clear"><!--fix --></div>
               </div><!--content-inner-wrapper -->
               </div><!--content-wrapper -->
            <?php print devavrata_free_bare_region($right_inside_wrapper, $region_id = 'right-wrapper', $region_class = 'sidebar-wrapper wrapper'); ?>
            <div class="block-clear"><!--fix --></div>         
            </div><!--content-wrapper -->
            <?php print devavrata_free_bare_region($footer_region, $region_id = 'footer-region', $region_class = 'footer-region block-outline block-panel sub-section'); ?>
            <?php print devavrata_free_bare_region($footer_bottom, $region_id = 'footer-region-bottom', $region_class = 'footer-region sub-section'); ?>
    		<div class="block-clear"><!--fix --></div>
    	</div><!--main-wrapper -->
    	<?php print devavrata_free_bare_region($right_outside_wrapper, $region_id = 'right-outside-wrapper', $region_class = 'sidebar-outside-wrapper wrapper main-section'); ?>
    	<div class="block-clear"><!--fix --></div>
	</div><!--page-wrapper --><div class="block-clear"><!--fix --></div>
<?php print $closure ?>
<?php print devavrata_free_bare_is_front_var($is_front); ?>
</body>
</html>
