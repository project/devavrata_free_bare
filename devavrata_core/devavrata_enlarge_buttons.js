/***********************************************************
 * Devavrata Core Theme for Drupal
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
/* enlarge buttons */
$(document).ready(function() {
	$('#content-normal').bind('click', function() {
		$('#content-region .content').removeClass('large').removeClass('larger');
	});
	$('#content-large').bind('click', function() {
		$('#content-region .content').removeClass('larger').addClass('large');
	});
	$('#content-larger').bind('click', function() {
		$('#content-region .content').removeClass('large').addClass('larger');
	});
	$('#content-switch .button').bind('click', function() {
		$('#content-switch .button').removeClass('selected');
		$(this).addClass('selected');
	});
});