<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - Free
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
$devavrata_path = drupal_get_path('theme','devavrata_free_bare'); 
// Initialize theme settings
function devavrata_initialize() {
	  global $theme_key;
	
	  $defaults = array( 
	// DEVAVRATA LAYOUT SETTINGS
		'devavrata_settings'							=> 0,
		'body_image' 									=> 0,
		'body_image_position' 						=> 0,
		'page_align' 									=> 0,
		'page_size' 									=> 2,
		'content_column' 								=> 0,
		'block_bg_title' 								=> 0,
		'block_panel' 									=> 0,
		'block_outline'							 	=> 0,
		'block_corner' 								=> 0,
		'block_corner_radius' 						=> 0,
		'block_spacing' 								=> 0,
		'sidebar_left_width' 						=> 0,
		'sidebar_left_position' 					=> 0,
		'sidebar_left_outside_width' 				=> 0,
		'sidebar_left_outside_position' 			=> 0,
		'sidebar_right_width' 						=> 0,
		'sidebar_right_position' 					=> 0,
		'sidebar_right_outside_width' 			=> 0,
		'sidebar_right_outside_position' 		=> 0,
		'content_image_alignment' 					=> 0,
	//   'sidebar_background_color' => 'White',
	//   'sidebar_background_image' => 'Off',
	
	// DEVAVRATA TEXT FORMAT SETTINGS
		'drop_cap' 										=> 0,
		'content_enlarge_button' 					=> 0,
		'content_title_size' 						=> 0,
		'content_title_align' 						=> 0,
		'content_body_size' 							=> 0,
		'content_body_margin' 						=> 0,
		'content_body_fonts' 						=> 0,
	
	// DEVAVRATA UTILITY SETTINGS
	//   'page_utility_button' => 'Hide',
		'header_hide' 									=> 0,
		'header_size' 									=> 0,
		'header_position' 							=> 0,
		'sitename_shadow' 							=> 0,
	//   'header_flash_version' => 'Off',
		'search_transfer' 							=> 0,
		'primary_menubar' 							=> 0,
		'secondary_menubar' 							=> 0,
		'block_icon' 									=> 0,
		'block_content' 								=> 0,
	//   'block_hover' => 'Off',
	//   'block_draggable' => 'No',
		);
	  
	  // Get default theme settings.
	  $settings = theme_get_settings($theme_key);
	  
	  // Don't save the toggle_node_info_ variables.
	  if (module_exists('node')) {
		 foreach (node_get_types() as $type => $name) {
			unset($settings['toggle_node_info_' . $type]);
		 }
	  }
	  // Save default theme settings.
	  variable_set(
		 str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
		 array_merge( $settings, $defaults)
	  );
	  // Force refresh of Drupal internals.
	  theme_get_setting('', TRUE);
}

// Page layout feature
function devavrata_free_bare_layout_feature($body_image = 0, $body_image_position = 0) {
	if($body_image || $body_image_position) {
		$layout_style = '<style type="text/css" media="screen">';
		switch($body_image) {
			case 0: $layout_style .= ''; break;
			case 1: $layout_style .= 'body { background-image: none; }'; break;		
			default: $layout_style .= ''; break;	
		}
		switch($body_image_position) {
			case 0: $layout_style .= ''; break;
			case 1: $layout_style .= 'body { background-attachment: scroll; }'; break;		
			default: $layout_style .= ''; break;	
		}
		$layout_style .= '</style>';
		return $layout_style;
	}
}

// Page size options
function devavrata_free_bare_width_settings($page_size = 0) {
	switch($page_size) {
		case 0: $page_size = ''; break;
		case 1: $page_size = '-800px'; break;		
		case 2: $page_size = '-600px'; break;		
		default: $page_size = ''; break;	
	}
	return $page_size;
}
// JQuery for collapsible blocks
if(theme_get_setting('block_content') == 1) {
	drupal_add_js($devavrata_path . '/devavrata_core/devavrata_block_content.js', 'theme');
}

/* Devavrata Text Features
 * content body fonts
 * content title size
 * content body size
 * dropcap Letter
 */
function devavrata_free_bare_text_feature($content_title_size = 0, $content_title_align = 0, $content_body_size = 0, $content_body_margin = 0) {
	if($content_title_size || $content_body_size || $content_body_margin) {
		$body_style = '<style type="text/css" media="screen">';
		switch($content_title_size) {
			case 0: $body_style .= ''; break;
			case 1: $body_style .= '#content-region h2, #content-elements h2 { font-size: 18px; }'; break;
			case 2: $body_style .= '#content-region h2, #content-elements h2 { font-size: 21px; }'; break;		
			case 3: $body_style .= '#content-region h2, #content-elements h2 { font-size: 25px; }'; break;
			default: $body_style .= ''; break;
		}
		switch($content_title_align) {
			case 0: $body_style .= ''; break;
			case 1: $body_style .= '#content-region .title, #content-elements .title { text-align: center; }'; break;
			case 2: $body_style .= '#content-region .title, #content-elements .title { text-align: right; }'; break;		
			default: $body_style .= ''; break;
		}
		switch($content_body_size) {
			case 0: $body_style .= ''; break;
			case 1: $body_style .= '#content-region .content { font-size: 12px; 	line-height: 16px;}'; break;
			case 2: $body_style .= '#content-region .content { font-size: 13px; 	line-height: 18px;}'; break;		
			case 3: $body_style .= '#content-region .content { font-size: 14px; 	line-height: 20px;}'; break;
			default: $body_style .= ''; break;		
		}
		switch($content_body_margin) {
			case 0: $body_style .= ''; break;
			case 1: $body_style .= '#content-region, #content-elements { margin: 10px; }'; break;
			case 2: $body_style .= '#content-region, #content-elements { margin: 15px; }'; break;		
			case 3: $body_style .= '#content-region, #content-elements { margin: 20px; }'; break;
			case 4: $body_style .= '#content-region, #content-elements { margin: 25px; }'; break;
			case 5: $body_style .= '#content-region, #content-elements { margin: 30px; }'; break;
			default: $body_style .= ''; break;		
		}
		$body_style .= '</style>';
		return $body_style;
	}
}
// Page utility feature
function devavrata_free_bare_utility_feature($block_content = 0) {
	if($block_content) {
		$utility_style = '<style type="text/css" media="screen">';
		switch($block_content) {
			case 0: $utility_style .= ''; break;
			case 1: $utility_style .= '.sidebar .block .content { display: none; }'; break;		
			default: $utility_style .= ''; break;	
		}
		
		$utility_style .= '</style>';
		return $utility_style;
	}	
}