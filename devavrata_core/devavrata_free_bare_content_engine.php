<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - Free
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
/* Process header content
 */
function devavrata_free_bare_header_region($logo, $site_name, $site_slogan, $search_box, $header, $front_page, $sitename_shadow = 0) {
	if ($logo || $site_name || $site_slogan || $header || $search_box) {
		$header_contents = '';
		if($logo || $site_name || $site_slogan) { 
			$header_contents .= '<div id="logo-block" class="logo-block">';
			if ($logo) { $header_contents .= '<div id="logo" class="logo-block"><a href="' . $front_page . '" title="' . t('Home') . '" rel="home" ><img src="' . $logo . '" alt="' . t('Home') . '" /></a></div>';};
			if($site_name || $site_slogan) { 
				$header_contents .= '<div id="name-slogan" class="logo-block">';
				if($site_name) { $header_contents .= '<div id="site-name" class="logo-block"><h1><a href="' . $front_page . '" title="' . t('Home') . '" rel="home">' . $site_name . '</a></h1></div>';}
				if($sitename_shadow == 0) { $header_contents .= '<div id="site-name-shadow"><h1>' . $site_name . '</h1></div>';}
				if($site_slogan) { $header_contents .= '<div class="block-clear"></div><div id="site-slogan" class="logo-block"><h3>' . $site_slogan . '</h3></div>';};
				$header_contents .= '<div class="block-clear"></div></div><!--name-slogan-->';
			};
			$header_contents .= '<div class="block-clear"></div>';
			$header_contents .= '</div><!--logo-block-->';
		}; // logo block
		if ($search_box) { $header_contents .= $search_box;}
		if ($header) { $header_contents .= '<div id="header" class="inner-section">' . $header . '</div>'; }
		$header_contents .= '<div class="block-clear"></div>';
		return $header_contents;
	}
}

/* #primary-links   => primary navigation bar with dropdown menu
 * #secondary-links => secondary navigation bar with dropdown menu
 */
function devavrata_free_bare_menubar_links($menu_links_tree, $menu_links, $menu_links_id, $menu_links_class) {
	if($menu_links_id == 'primary-links') {
		if($menu_links) {
			$menubar = '<div id="' . $menu_links_id . '" class="'. $menu_links_class . '">'; 
			$menubar .= '<div id="home-icon" class="primary-link-icon"><a href="http://www.devavrata.com" title="' . t('Home') . '" rel="home" ><img src="/sites/all/themes/devavrata_free_bare/icons/icon_gray.gif" alt="' . t('Home') . '" /></a></div>';
			//$menubar .= '<div id="email-icon" class="primary-link-icon"><a href="/contact" title="' . t('Email') . '" rel="email" ><img src="/sites/all/themes/devavrata_free_bare/icons/email-16.gif" alt="' . t('Email') . '" /></a></div>';
			//$menubar .= '<div id="rss-icon" class="primary-link-icon"><a href="/rss.xml" title="' . t('RSS') . '" rel="RSS" ><img src="/sites/all/themes/devavrata_free_bare/icons/rss-16.gif" alt="' . t('RSS') . '" /></a></div>';
			$menubar .= '<div id="devavrata-link" class="devavrata-link"></div>';
			$menubar .= $menu_links_tree; 
			$menubar .= '<div class="block-clear"></div></div>'; 
			return $menubar;
		}
	} else {
		if($menu_links) {
			$menubar = '<div id="' . $menu_links_id . '" class="' . $menu_links_class . '">'; 
			$menubar .= $menu_links_tree; 
			$menubar .= '<div class="block-clear"></div></div>'; 
			return $menubar;
		}
	}
}

/* Generate content breadcrumb, mission, title, tabs, messages, help
 */
function devavrata_free_bare_content_elements($breadcrumb,$mission,$title,$tabs,$tabs2,$messages,$help) {
	if ($breadcrumb || $mission || $title || $tabs || $tabs2 || $messages || $help) {
		$content_elements = '<div id="content-elements" class="content-elements sub-section">';
		if ($breadcrumb) { $content_elements .= '<div id="breadcrumb" class="inner-section">' . $breadcrumb . '</div>'; };
		if ($mission) { $content_elements .= '<div id="mission">' . $mission . '</div>'; }; 
		if ($title) { $content_elements .= '<h2 class="title node-title">' . $title . '</h2>'; };
		if ($tabs) { $content_elements .= '<ul class="tabs primary">' . $tabs . '</ul>'; };  
		if ($tabs2) { $content_elements .= '<ul class="tabs secondary">'. $tabs2 .'</ul>'; };
		if ($messages) { $content_elements .= $messages; };    
		if ($help) { $content_elements .= $help; };
		$content_elements .= '</div>';
		return $content_elements;
	}
}


/* Process footer content
 */
function devavrata_free_bare_footer($footer_message, $footer) { //$feed_icons, 
	if($footer_message || $footer) { //$feed_icons || 
		if ($footer_message) { $footer_content = '<div id="footer-message">' . $footer_message . '</div>'; }
		//if ($feed_icons) { $footer_content .= '<div id="footer-icons">' . $footer_icons . '</div>'; }
		if ($footer) { $footer_content .= '<div id="footer">' . $footer . '</div>'; }
		return $footer_content;
	}
}

/**
 * Override or insert devavrata variables into the templates.
 */
function devavrata_free_bare_preprocess_page(&$vars) {
	$vars['tabs2'] = menu_secondary_local_tasks();
	// Generate menu tree from source of primary links
	$vars['primary_links_tree'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
	$vars['secondary_links_tree'] = menu_tree(variable_get('menu_secondary_links_source', 'secondary-links'));
	// Hook into color.module
	if (module_exists('color')) {
		_color_page_alter($vars);
	}
	// on devavrata theme
	$vars['closure'] .= '<div id="legal">Devavrata Core Theme ' . date('Y') . ' | a Multi-Block System | by <a href="http://danielhonrade.com">Daniel Honrade</a> under GPL license from <a href="http://webtheming.com/">WebTheming.com Drupal Themes</a></div>';
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function devavrata_free_bare_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">go back to >> '. implode(' >> ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function devavrata_free_bare_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><div class="block-title"> <h2 class="comments">'. t('Comments') .'</h2></div>'. $content .'</div>';
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function devavrata_free_bare_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function devavrata_free_bare_comment_submitted($comment) {
  return t('!datetime | !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function devavrata_free_bare_node_submitted($node) {
  return t('!datetime | by !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}


/* Generate elements on the following variables
 * $is_front   => if the current page is frontpage
 * $logged_in  => if the user is logged in
 * $is_admin   => if the user is the user 1
 */
function devavrata_free_bare_is_front_var($is_front) {
	if ($is_front) { return  '<div id="front-page">DFBT 6.x-1.0</div>'; }
}
function devavrata_free_bare_logged_in_var($logged_in) {
	if ($logged_in) { print '<div id="user-logged-in"><h2>YOU ARE LOGGED IN, <a href="/logout">Click</a> here to logout.</h2></div>'; }
	else { print '<div id="user-logged-out"><!--Your are not logged in.--></div>';}
}
function devavrata_free_bare_is_admin_var($is_admin) {
	if ($is_admin) { print '<div id="user-admin"><h1>ADMINISTRATOR PAGES</h1></div>'; }
}

function devavrata_free_bare_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Special classes for blocks.
  $classes = array('block');
  $classes[] = 'block-' . $block->module;
  $classes[] = 'region-' . $vars['block_zebra'];
  $classes[] = $vars['zebra'];
  $classes[] = 'region-count-' . $vars['block_id'];
  $classes[] = 'count-' . $vars['id'];

  $vars['edit_links_array'] = array();
  $vars['edit_links'] = '';
  if (user_access('administer blocks')) {
    include_once './' . drupal_get_path('theme', 'devavrata_free_bare') . '/devavrata_core/template.block-editing.inc';
    phptemplate_preprocess_block_editing($vars, $hook);
    $classes[] = 'with-block-editing';
  }

  // Render block classes.
  $vars['classes'] = implode(' ', $classes);
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
 
function devavrata_free_bare_get_ie6_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/css/ie6.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/css/ie-rtl.css";</style>';
  }
  return $iecss;
}
function devavrata_free_bare_get_ie7_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/css/ie7.css" />';
  if ($language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="all">@import "'. base_path() . path_to_theme() .'/css/ie-rtl.css";</style>';
  }
  return $iecss;
}
