<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - 4-Column Style
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
/* Generate column regions
 */
function devavrata_free_bare_column_region($column1 = '', $column2 = '', $column3 = '', $column4 = '', $column_region = '', $is_admin, $column_state = '', $page_width_setting = '') {
	if (empty($column_state)) { 
		$column_state = 'column-hide'; // hide guide
		$show_hide = ''; // button value
	} elseif($useraccess=='danielhonrade') { 
		$column_state = ''; // show guide
		$show_hide = ''; // button value
	}
	if ($column1 || $column2 || $column3 || $column4) { 
		if ($is_admin) { // guide table
			$column_guide = '<div id="column-guide" class="block-outline block-panel sub-section column-guide ' . $column_state . '">';
			$column_guide .= '<div class="block"><div class="block-title"><h2>USE ONLY THE FOLLOWING COLUMN(S) TO GET THE DESIRED WIDTH(S)</h2></div></div>';
			$column_guide .= '<table border="0" width="998" cellspacing="5" class="devavrata-column-guide">';
			$column_guide .= '<tr><td colspan="12"><div class="block-region">' . $column_region . ' Left Most | Left | Right | Right Most</div></td></tr>';
			$column_guide .= '<tr><td colspan="6"><div class="block-region">' . $column_region . ' Column Left Most | Left</div></td><td colspan="6"><div class="block-region">' . $column_region . ' Column Right | Right Most</div></td></tr>';
			$column_guide .= '<tr><td  width="25%" colspan="3"><div class="block-region">' . $column_region . ' Left Most</div></td><td  width="25%" colspan="3"><div class="block-region">' . $column_region . ' Left</div></td><td  width="25%" colspan="3"><div class="block-region">' . $column_region . ' Left</div></td><td width="25%" colspan="3"><div class="block-region">' . $column_region . ' Right Most</div></td>';
			$column_guide .= '<tr><td colspan="3"><div class="block-region">' . $column_region . ' Left Most</div></td><td colspan="3"><div class="block-region">' . $column_region . ' Left</div></td><td colspan="6"><div class="block-region">' . $column_region . ' Right Most</div></td>';
			$column_guide .= '<tr><td colspan="6"><div class="block-region">' . $column_region . ' Left Most</div></td><td colspan="3"><div class="block-region">' . $column_region . ' Right</div></td><td colspan="3"><div class="block-region">' . $column_region . ' Right Most</div></td>';
			$column_guide .= '<tr><td colspan="3"><div class="block-region">' . $column_region . ' Left</div></td><td colspan="6"><div class="block-region">' . $column_region . ' Right</div></td><td colspan="3"><div class="block-region">' . $column_region . ' Right Most</div></td>';
			$column_guide .= '<tr><td colspan="3"><div class="block-region">' . $column_region . ' Left Most</div></td><td colspan="9"><div class="block-region">' . $column_region . ' Right</div></td></tr>';
			$column_guide .= '<tr><td colspan="9"><div class="block-region">' . $column_region . ' Left</div></td><td colspan="3"><div class="block-region">' . $column_region . ' Right Most</div></td></tr>';
			$column_guide .= '<tr><td colspan="4"><div class="block-region">' . $column_region . ' Left Most</div></td><td colspan="4"><div class="block-region">' . $column_region . ' Left</div></td><td colspan="4"><div class="block-region">' . $column_region . ' Right</div></td>';
			$column_guide .= '<tr><td colspan="4"><div class="block-region">' . $column_region . ' Left Most</div></td><td colspan="8"><div class="block-region">' . $column_region . ' Left</div></td></tr>';
			$column_guide .= '<tr><td colspan="8"><div class="block-region">' . $column_region . ' Right</div></td><td colspan="4"><div class="block-region">' . $column_region . ' Right Most</div></td></tr>';
			$column_guide .= '</table>'; 			
			$column_guide .= '<div class="block-clear"></div>'; 			
			$column_guide .= '</div>';
			
			$column_guide .= '<form action="' . $SCRIPT_NAME . '" method="get">'; 
			$column_guide .= '<strong>YOU HAVE ADMINISTRATIVE ACCESS.';
			$column_guide .= 'BE SURE TO LOGOUT PROPERLY AND CLEAR THE CACHE OF THIS BROWSER.</strong>';
			$column_guide .= '<input id="useraccess" type="textfield" name="column-state" value="' . $useraccess . '">'; 
			$column_guide .= '<input id="submit-useraccess" type="submit" name="submit-useraccess" value="' . $show_hide . '"><br />'; 
			$column_guide .= '</form>';
			
			print $column_guide;
		} 

		$column_counter = 0; // intialize column counter for final column number class
		if ($column1) { $column_counter++; }; // column 1 named column-1
		if ($column2) { $column_counter++; }; // column 1 & 2 named column-2
		if ($column3) { $column_counter++; }; // column 1,2 & 3 named column-3
		if ($column4) { $column_counter++; }; // column 1,2,3 & 4 named column-4
		
		if ((!$column1 && $column2 && !$column3 && $column4) || ($column1 && !$column2 && $column3 && !$column4)) { $column_counter = 4; $column_middle1 = 'column-widest'; $column_middle2 = 'column-widest'; } // column 1 (25%) + column 3 (75%) or column 2 (75%) + column 4 (25%)
		elseif (!$column1 && $column2 && $column3 && $column4) { $column_counter = 4; $column_middle2 = 'column-wide'; } // column 2 & 4 (25%) + column 3 (50%) 
		elseif ($column1 && !$column2 && $column3 && $column4) { $column_counter = 4; $column_first = 'column-wide'; } // column 1 (25%), 2 (25%) + column 4 (50%)
		elseif ($column1 && $column2 && !$column3 && $column4) { $column_counter = 4; $column_middle3 = 'column-wide'; } // column 1 (50%) + column 3 (25%) & 4 (25%)
		elseif ($column1 && ((!$column2 && $column3) || ($column2 && !$column3)) && $column4 ) { $column_counter = 4; $column_middle2 = 'column-wide'; $column_middle3 = 'column-wide'; } // column 1 (25%)column 2 or 3 (50%) + column 4 (25%)
		elseif (!$column1 && !$column2 && $column3 && $column4) { $column_counter = 3; $column_middle2 = 'column-wider'; } // column 3 (66%) + column 4 (33%)
		elseif ($column1 && $column2 && !$column3 && !$column4) { $column_counter = 3; $column_middle1 = 'column-wider'; } // column 1 (33%) + column 2 (66%)
		else { $column_first = ''; $column_middle1 = ''; $column_middle2 = ''; $column_middle3 = ''; $column_last = ''; }; // columns back to equal sizes if no above conditions met

		$column_count_final = $column_counter; // final column number class
		$output = '<div id="' . $column_region .'" class="column-region block-outline block-panel sub-section">';
		
		if ($column1) { $output .= '<div id="' . $column_region . '-col1" class="column-' . $column_count_final . $column_first . $page_width_setting . ' column inner-section">' . $column1 .'</div>'; };
		if ($column2) { $output .= '<div id="' . $column_region . '-col2" class="column-' . $column_count_final . $column_middle1 . $page_width_setting . ' column inner-section">' . $column2 .'</div>'; };
		if ($column3) { $output .= '<div id="' . $column_region . '-col3" class="column-' . $column_count_final . $column_middle2 . $page_width_setting . ' column inner-section">' . $column3 .'</div>'; };
		if ($column4) { $output .= '<div id="' . $column_region . '-col4" class="column-' . $column_count_final . $column_middle3 . $page_width_setting . ' column inner-section">' . $column4 .'</div>'; };
		$output  .= '<div class="block-clear"></div></div>';
		return $output;
	};
}