Drupal Theme	: DEVAVRATA FREE THEME
Theme Type		: CORE THEME
Developer		: Daniel Honrade
Contact			: mail@danielhonrade.com
URL				: http://www.danielhonrade.com/ | http://www.webtheming.com
Current Version	: 6.x-1.0
Update Date		: 06/09/09
Core Features	: - 14-Block Sytem (listed below
				  - 5px margin for panels (regions)
				  - All panels (regions) are collapsible including the header region
			  	    except the content region
				  - Column widths are optimized for adsense ads
				  - Default font sizes are optimized for heavy content layout
			  	    such as e-commerce, e-publishing, e-catalogues, e-news, wikkis

My Message		: YOU CAN CUSTOMIZE OR CREATE SUBTHEMES/SKINS (instructions are
			      in the custom.css file)
			
				  I am constantly updating this core theme, so if you
				  want to upload my updates for this core theme,
				  please do not make your changes on the core theme files
			
				  IF YOU WANT ME TO CUSTOMIZE THIS THEME FOR YOU,
				  YOU CAN SEND ME AN EMAIL & I'LL GIVE YOU A REASONABLE QUOTE

No. of Blocks	: 14
Blocks List		: HEADER REGION
					1) Header
					2) Header Bottom
				  					
				  PREFACE
					3) Preface Left Most
					4) Preface Left
					5) Preface Right
					6) Preface Right Most
					
				  REGULAR CONTENT
					7) Content Top
					8) Content
					
				  FOOTER
					9) Footer
					10)Footer Bottom

				  SIDEBARS
					11)Left Sidebar
					12)Left Sidebar Inside
					13)Right Sidebar
					14)Right Sidebar Inside
