<?php
/***********************************************************
 * Devavrata Core Theme for Drupal
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
/**
 * @file block.tpl.php
 *
 * Theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $block->content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: This is a numeric id connected to each module.
 * - $block->region: The block region embedding the current block.
 *
 * Helper variables:
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>
<?php 
 	$block_content = theme_get_setting('block_content');	

	print '<div id="block-' . $block->module .'-'. $block->delta . '" class="block block-' . $block->module . '">';
	if($block_content == 1) { print '<a href="#">'; }
	if ($block->subject){ print '<div class="block-title"><div class="block-title-begin"></div><div class="block-title-end"></div><h2>' . $block->subject . '</h2></div>';}
	if($block_content == 1) { print '</a>'; }
	print $edit_links;
 	print '<div  class="block-clear"></div><div class="content block-outline block-panel">' . $block->content . '</div></div>';
	