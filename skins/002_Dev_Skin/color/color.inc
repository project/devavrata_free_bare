<?php

$info = array(

  // Pre-defined color schemes.
  'schemes' => array(
    '#0072b9,#027ac6,#2385c2,#ffffff,#494949' => t('Blue Lagoon (Default)'),
    '#464849,#2f416f,#2a2b2d,#5d6779,#494949' => t('Ash'),
    '#55c0e2,#000000,#085360,#007e94,#696969' => t('Aquamarine'),
    '#611600,#6c420e,#331900,#971702,#494949' => t('Belgian Chocolate'),
    '#3f3f3f,#336699,#6598cb,#6598cb,#000000' => t('Bluemarine'),
    '#d0cb9a,#917803,#efde01,#e6fb2d,#494949' => t('Citrus Blast'),
    '#0f005c,#434f8c,#4d91ff,#1a1575,#000000' => t('Cold Day'),
    '#0c6600,#0c7a00,#03961e,#7be000,#494949' => t('Greenbeam'),
    '#ffe23d,#a9290a,#fc6d1d,#a30f42,#494949' => t('Mediterrano'),
    '#788597,#3f728d,#a9adbc,#d4d4d4,#707070' => t('Mercury'),
    '#5b5fa9,#5b5faa,#0a2352,#9fa8d5,#494949' => t('Nocturnal'),
    '#7db323,#6a9915,#b5d52a,#7db323,#191a19' => t('Olivia'),
    '#12020b,#1b1a13,#f391c6,#f41063,#898080' => t('Pink Plastic'),
    '#b7a0ba,#c70000,#a1443a,#f21107,#515d52' => t('Shiny Tomato'),
    '#18583d,#1b5f42,#34775a,#52bf90,#2d2d2d' => t('Teal Top'),
  ),

  // Images to copy over.
    'copy' => array(
    'images/bg_bar.png',
    'images/bg_bar_dark.png',
	'images/bg_bar_second.png',
	'images/menu-expanded.png',
	'images/menu-collapsed-rtl.png'
 ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/backgrounds.css','css/texts.css','css/menus.css'
  ),

  // Coordinates of gradient (x, y, width, height).
   'gradient' => array(0, 0, 5, 700),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 126, 500, 330),
    'link' => array(107, 533, 41, 23),
	'text' => array(0, 0, 5, 5),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/background.png'         => array(0, 0, 5, 700),
     'images/bg_bar_dark.png'       => array(80, 128, 5, 50),
     'images/bg_bar.png'            => array(60, 127, 5, 50),
     'images/bg_bar_long.png'       => array(20, 126, 5, 330),
     'images/bg_bar_long_dark.png'  => array(40, 126, 5, 330),
     'images/bg_bar_second.png'     => array(90, 135, 5, 50),
     'images/bg_bar_dev.png'        => array(104, 128, 300, 25),
     'logo.png'                     => array(113, 16, 100, 100),
 ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
    'preview_image' => 'color/preview.png',
    'preview_css' => 'color/preview.css',

  // Base file for image generation.
    'base_image' => 'color/base.png',
);
