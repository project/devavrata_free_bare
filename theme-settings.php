<?php
/***********************************************************
 * Devavrata Core Theme for Drupal - Free Bare
 * A WebTheming.com Stylesheet Production
 * Created by Daniel Honrade
 * for paid technical support: mail@danielhonrade.com
 * http://webtheming.com
 * http://danielhonrade.com
 ***********************************************************/
 
function phptemplate_settings($saved_settings) {
	
  // Add javascript to show/hide optional settings
  drupal_add_css(drupal_get_path('theme', 'devavrata_free_bare') . '/devavrata_core/devavrata_settings.css', 'theme', 'all');	
  // Add javascript to show/hide optional settings
  drupal_add_js(drupal_get_path('theme', 'devavrata_free_bare') . '/devavrata_core/devavrata_settings.js', 'theme');		
  $defaults = array(

// DEVAVRATA LAYOUT SETTINGS
	'devavrata_settings' 						=> 0,
	'body_image' 									=> 0,
	'body_image_position' 						=> 0,
	'page_align' 									=> 0,
   'page_size' 									=> 2,
   'content_column' 								=> 0,
	'block_bg_title' 								=> 0,
	'block_panel' 									=> 0,
	'block_outline'							 	=> 0,
	'block_corner' 								=> 0,
	'block_corner_radius' 						=> 0,
   'block_spacing' 								=> 0,
   'sidebar_left_width' 						=> 0,
   'sidebar_left_position' 					=> 0,
   'sidebar_left_outside_width' 				=> 0,
   'sidebar_left_outside_position' 			=> 0,
   'sidebar_right_width' 						=> 0,
   'sidebar_right_position' 					=> 0,
   'sidebar_right_outside_width' 			=> 0,
   'sidebar_right_outside_position' 		=> 0,
   'content_image_alignment' 					=> 0,
//   'sidebar_background_color' => 'White',
//   'sidebar_background_image' => 'Off',

// DEVAVRATA TEXT FORMAT SETTINGS
   'drop_cap' 										=> 0,
   'content_enlarge_button' 					=> 0,
   'content_title_size' 						=> 0,
   'content_title_align' 						=> 0,
   'content_body_size' 							=> 0,
	'content_body_margin' 						=> 0,
   'content_body_fonts' 						=> 0,

// DEVAVRATA UTILITY SETTINGS
//   'page_utility_button' => 'Hide',
   'header_hide' 									=> 0,
	'header_size' 									=> 0,
	'header_position' 							=> 0,
	'sitename_shadow' 							=> 0,
//   'header_flash_version' => 'Off',
	'search_transfer' 							=> 0,
	'primary_menubar' 							=> 0,
	'secondary_menubar' 							=> 0,
	'block_icon' 									=> 0,
	'block_content' 								=> 0,
//   'block_hover' => 'Off',
//   'block_draggable' => 'No',
);
  
$settings = array_merge($defaults, $saved_settings);

//devavrata settings
  $form['devavrata_settings'] = array(
    '#type' => 'checkbox',
    '#title' => t('<strong>Use Devavrata Core Settings</strong>'),
    '#default_value' => $settings['devavrata_settings'],
	//'#options' => array('No','Yes'),
 	'#attributes'    => array('id' => 'devavrata-settings'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  
 // DEVAVRATA Fieldsets - 1st level
  $form['dev_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('For feature request or bug report contact:'),
    '#description' => t('<div align="center"><h3><a href="http://www.danielhonrade.com" target="_blank">Daniel Honrade</a> | <a href="http://www.danielhonrade.com/contact" target="_blank">mail@danielhonrade.com</a></h3></div>'),
    //'#collapsible' => TRUE,
    //'#collapsed' => TRUE,
	 '#prefix' => '<div id="devavrata-settings-collapse">',
	 '#suffix' => '</div>',
	); 
 // DEVAVRATA Fieldsets - 2nd level
  $form['dev_container']['dev_layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Devavrata Layout Settings'),
    '#description' => t('Change Devavrata layout with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_text_format'] = array(
    '#type' => 'fieldset',
    '#title' => t('Devavrata Text Format Settings'),
    '#description' => t('Change Devavrata Text Formating with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_utility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Devavrata Utility Settings'),
    '#description' => t('Change other Devavrata functionalities with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
 // DEVAVRATA Fieldsets - 3rd level
  $form['dev_container']['dev_layout']['dev_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page'),
    '#description' => t('Change page specifications with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_layout']['dev_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main Content'),
    '#description' => t('Change main content specifications with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_layout']['dev_blocks'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocks/Regions'),
    '#description' => t('Change the looks of your blocks with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_layout']['dev_sidebars'] = array(
    '#type' => 'fieldset',
    '#title' => t('Side Bars'),
    '#description' => t('Change the widths of the side bars with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_utility']['dev_header_utility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Header Utilities'),
    '#description' => t('Customize your header with these settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  $form['dev_container']['dev_utility']['dev_other_utility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other Utilities'),
    '#description' => t('Enhance user experience with these settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ); 
  
  
// DEVAVRATA LAYOUT SETTINGS

//body image
  $form['dev_container']['dev_layout']['dev_page']['body_image'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide background image'),
    '#default_value' => $settings['body_image'],
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //body image position
  $form['dev_container']['dev_layout']['dev_page']['body_image_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Scroll background image'),
    '#default_value' => $settings['body_image_position'],
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //page alignment
  $form['dev_container']['dev_layout']['dev_page']['page_align'] = array(
    '#type' => 'radios',
    '#title' => t('Set the page alignment'),
    '#default_value' => $settings['page_align'],
	'#options' => array('Center','Left','Right'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//page width 600px, 800px, 1000px
  $form['dev_container']['dev_layout']['dev_page']['page_size'] = array(
    '#type' => 'radios',
    '#title' => t('Set the page width (this size excludes the outside side bars)'),
    '#default_value' => $settings['page_size'],
	'#options' => array('1000px','800px','600px'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//page style, default or column
 if($settings['page_size'] != 0) {
  	$form['dev_container']['dev_layout']['dev_content']['content_column'] = array(
		'#type' => 'radios',
		'#title' => t('Main content layout style (Change page size to 1000px page size to use 5-Content Column Style)'),
		'#value' => 0,
		'#options' => array('Default Style','5-Content Column Style'),
		'#disabled' => 'disabled', //Do not enable if your version does not support this feature or your page size is not set to 1000px	 
  	);
 } else {
  	$form['dev_container']['dev_layout']['dev_content']['content_column'] = array(
		'#type' => 'radios',
		'#title' => t('Main content layout style (5-Content Column Style only works for 1000px page size)'),
		'#default_value' => $settings['content_column'],
		'#options' => array('Default Style','5-Content Column Style'),
		'#disabled' => 'disabled', //Do not enable if your version does not support this feature
    );
 }
//node image alignment
  $form['dev_container']['dev_layout']['dev_content']['content_image_alignment'] = array(
    '#type' => 'radios',
    '#title' => t('Set the alignment of the image in the main content'),
    '#default_value' => $settings['content_image_alignment'],
	'#options' => array('Right','Left','Center'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
 //block background title regular/modern
  $form['dev_container']['dev_layout']['dev_blocks']['block_bg_title'] = array(
    '#type' => 'select',
    '#title' => t('Change the background of the block title'),
    '#default_value' => $settings['block_bg_title'],
	'#options' => array('default','Black','White','Gray','Metallic','Gold', 'Red','Orange','Yellow','Apple Green','Green',
							  'Blue', 'Violet', 'Aqua Marine', 'Pink', 'Cyan'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
 //block panel
  $form['dev_container']['dev_layout']['dev_blocks']['block_panel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn off white block panel'),
    '#default_value' => $settings['block_panel'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //block outline
  $form['dev_container']['dev_layout']['dev_blocks']['block_outline'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn off block outline'),
    '#default_value' => $settings['block_outline'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//block corner
  $form['dev_container']['dev_layout']['dev_blocks']['block_corner'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn off block round corner'),
    '#default_value' => $settings['block_corner'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//block corner radius
  $form['dev_container']['dev_layout']['dev_blocks']['block_corner_radius'] = array(
    '#type' => 'select',
    '#title' => t('Block round corner radius'),
    '#default_value' => $settings['block_corner_radius'],
	'#options' => array('5px','10px','15px','20px','25px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//block spacing
  $form['dev_container']['dev_layout']['dev_blocks']['block_spacing'] = array(
    '#type' => 'select',
    '#title' => t('Block spacing'),
    '#default_value' => $settings['block_spacing'],
	'#options' => array('5px','10px','15px','20px','25px','30px','0px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar left
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_left_width'] = array(
    '#type' => 'select',
    '#title' => t('Left sidebar width'),
    '#default_value' => $settings['sidebar_left_width'],
	'#options' => array('180px','120px','150px','210px','240px','270px','300px','330px','360px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar left position
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_left_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed Position'),
    '#default_value' => $settings['sidebar_left_position'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //sidebar left outside
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_left_outside_width'] = array(
    '#type' => 'select',
    '#title' => t('Left outside sidebar width'),
    '#default_value' => $settings['sidebar_left_outside_width'],
	'#options' => array('180px','120px','150px','210px','240px','270px','300px','330px','360px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar left outside position
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_left_outside_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed Position'),
    '#default_value' => $settings['sidebar_left_outside_position'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //sidebar right
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_right_width'] = array(
    '#type' => 'select',
    '#title' => t('Right sidebar width'),
    '#default_value' => $settings['sidebar_right_width'],
	'#options' => array('180px','120px','150px','210px','240px','270px','300px','330px','360px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar right position
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_right_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed Position'),
    '#default_value' => $settings['sidebar_right_position'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar right outside
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_right_outside_width'] = array(
    '#type' => 'select',
    '#title' => t('Right outside sidebar width'),
    '#default_value' => $settings['sidebar_right_outside_width'],
	'#options' => array('180px','120px','150px','210px','240px','270px','300px','330px','360px'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//sidebar right outside position
  $form['dev_container']['dev_layout']['dev_sidebars']['sidebar_right_outside_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed Position'),
    '#default_value' => $settings['sidebar_right_outside_position'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  
// DEVAVRATA TEXT FORMAT SETTINGS
  //dropcap letter
  $form['dev_container']['dev_text_format']['drop_cap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn on dropcap (if your first paragraph does not immediately follow your title, this will not work, a work around is by putting a class="dropcap" on the paragraph you want to have it.'),
    '#default_value' => $settings['drop_cap'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//node body title size
  $form['dev_container']['dev_text_format']['content_title_size'] = array(
    '#type' => 'select',
    '#title' => t('Set the size of your body titles'),
    '#default_value' => $settings['content_title_size'],
	'#options' => array('16px','18px','21px','25px'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//node body title align
  $form['dev_container']['dev_text_format']['content_title_align'] = array(
    '#type' => 'radios',
    '#title' => t('Set the alignment of your body titles'),
    '#default_value' => $settings['content_title_align'],
	'#options' => array('Left','Center','Right'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//node body text size
  $form['dev_container']['dev_text_format']['content_body_size'] = array(
    '#type' => 'select',
    '#title' => t('Set the size of your body text'),
    '#default_value' => $settings['content_body_size'],
	'#options' => array('11px','12px','13px','14px'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//node body margin
  $form['dev_container']['dev_text_format']['content_body_margin'] = array(
    '#type' => 'select',
    '#title' => t('Set the margin of your body text'),
    '#default_value' => $settings['content_body_margin'],
	'#options' => array('5px','10px','15px','20px','25px','30px'),
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//over-all font type, 13 choices
  $form['dev_container']['dev_text_format']['content_body_fonts'] = array(
    '#type' => 'select',
    '#title' => t('Set body fonts'),
    '#default_value' => $settings['content_body_fonts'],
	'#options' => array('Arial, Helvetica, sans-serif',
						'"Times New Roman", Times, serif',
						'"Comic Sans MS", cursive',
						'"Courier New", Courier, monospace',
						'Verdana, Geneva, sans-serif',
						'Tahoma, Geneva, sans-serif',
						'"Trebuchet MS", Arial, Helvetica, sans-serif',
						'"Arial Black", Gadget, sans-serif',
						'"Lucida Sans Unicode", "Lucida Grande", sans-serif',
						'"Palatino Linotype", "Book Antiqua", Palatino, serif',
						'Georgia, "Times New Roman", Times, serif',
						'"MS Serif", "New York", serif',
						'"Lucida Console", Monaco, monospace',
						),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );

// DEVAVRATA UTILITY SETTINGS
//hide header in frontpage
  $form['dev_container']['dev_utility']['dev_header_utility']['header_hide'] = array(
    '#type' => 'radios',
    '#title' => t('Header (where the default Logo, Site Name, Slogan and Search are)'),
    '#default_value' => $settings['header_hide'],
	'#options' => array('Show in all pages','Show in all pages except in front','Show only in front','Hide in all pages'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//header size
  $form['dev_container']['dev_utility']['dev_header_utility']['header_size'] = array(
    '#type' => 'radios',
    '#title' => t('Set the header size'),
    '#default_value' => $settings['header_size'],
	'#options' => array('Normal','Narrow/Inside (above the main content)','Wide/Outside (above the outside bars)'),
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//header position
  $form['dev_container']['dev_utility']['dev_header_utility']['header_position'] = array(
    '#type' => 'checkbox',
    '#title' => t('Set the header in fixed position (stays even while scrolling)'),
    '#default_value' => $settings['header_position'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );  
//sitename shadow
  $form['dev_container']['dev_utility']['dev_header_utility']['sitename_shadow'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn off shadow effect on the site name'),
    '#default_value' => $settings['sitename_shadow'],
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );  
//transfer search box from header to menubar
  $form['dev_container']['dev_utility']['dev_header_utility']['search_transfer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transfer search box to menu bar'),
    '#default_value' => $settings['search_transfer'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//primary menubar
  $form['dev_container']['dev_utility']['dev_header_utility']['primary_menubar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide primary menu bar'),
    '#default_value' => $settings['primary_menubar'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
//secondary menubar
  $form['dev_container']['dev_utility']['dev_header_utility']['secondary_menubar'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide secondary menu bar'),
    '#default_value' => $settings['secondary_menubar'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );
  //hide block-icon
  $form['dev_container']['dev_utility']['dev_other_utility']['block_icon'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide icons'),
    '#default_value' => $settings['block_icon'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  ); 
//hide block content
  $form['dev_container']['dev_utility']['dev_other_utility']['block_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Make all blocks collapsed and show contents by clicking the block headers'),
    '#default_value' => $settings['block_content'],
	//'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  ); 
//enlarge button for body content text
  $form['dev_container']['dev_utility']['dev_other_utility']['content_enlarge_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show enlarge body text button'),
    '#default_value' => $settings['content_enlarge_button'],
	'#disabled' => 'disabled', //Do not enable if your version does not support this feature
  );  
  // Return the additional form widgets
  return $form;
}
?>
